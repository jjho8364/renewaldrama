package com.sothing.renewaldrama.activity;

import android.arch.lifecycle.LifecycleActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.sothing.renewaldrama.R;

public class VideoPlayerActivity extends LifecycleActivity {
    private static String TAG = "VideoPlayerActivity";
    private String fileType = "";
    private String videoUrl = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_player);
        SimpleExoPlayerView playerView = findViewById(R.id.simple_exoplayer_view);

        fileType = getIntent().getStringExtra("fileType");
        videoUrl = getIntent().getStringExtra("videoUrl");


        ImageView loadingImg = (ImageView)findViewById(R.id.gif_load);
        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(loadingImg);
        Glide.with(this).load(R.drawable.loding).into(gifImage);

        getLifecycle().addObserver(new VideoPlayerComponent(getApplicationContext(), playerView, fileType, videoUrl, loadingImg));

    }
}

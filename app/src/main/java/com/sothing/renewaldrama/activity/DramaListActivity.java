package com.sothing.renewaldrama.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sothing.renewaldrama.R;
import com.sothing.renewaldrama.adapter.DramaBtnAdapter;
import com.sothing.renewaldrama.adapter.GridDramaAdapter;
import com.sothing.renewaldrama.item.GridDramaItem;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class DramaListActivity extends Activity {
    private String TAG = " DramaListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;

    private ArrayList<GridDramaItem> listViewItemArr;
    private GridView gridView;
    private ListView btnListView;

    private String nextUrl = "";
    private GetPlayer getPlayer;

    private int adsCnt = 0;
    private String listUrl = "";

    ///// webview ////
    private WebView webView;
    //private myWebViewClient mWebViewClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_drama_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);

        tv_title.setText(title);
        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        gridView = (GridView)findViewById(R.id.gridview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        //////////// start contents //////////
        getGridView = new GetGridView();
        getGridView.execute();

    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(DramaListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                Elements lists = doc.select(".td_block_inner .td-related-span4");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select(".entry-title.td-module-title").text();
                    String update = "";
                    String imgUrl = lists.get(i).select(".entry-thumb").attr("src");
                    String listUrl = lists.get(i).select("a.td-image-wrap").attr("href");

                    GridDramaItem gridDramaItem = new GridDramaItem(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridDramaItem);
                }

                /////////////// video button //////////////
                Elements btns = doc.select(".linkcontents li");

                for(int i=0 ; i<btns.size() ; i++){
                    String text = btns.get(i).select(".linkboxt").text();
                    String videoUrl = btns.get(i).select(".linkshow").attr("href");

                    if(videoUrl.contains(".daum.net/")) continue;
                    if(videoUrl.contains("hqvid.net")) continue;
                    if(videoUrl.contains("goovid.net")) continue;
                    //if(videoUrl.contains("openload")) continue;
                    btnTextArr.add(text);
                    btnVideoUrlArr.add(videoUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(DramaListActivity.this != null && listViewItemArr.size() != 0){
                gridView.setAdapter(new GridDramaAdapter(DramaListActivity.this, listViewItemArr, R.layout.item_grid_drama));

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        adsCnt++;
                        Log.d(TAG, "adsCnt : " + adsCnt);
                        listUrl = listViewItemArr.get(position).getListUrl();
                        title = listViewItemArr.get(position).getTitle();
                        imgUrl = listViewItemArr.get(position).getImgUrl();
                        refresh(listUrl);
                    }
                });
            }

            ///////// set button list ////////////
            if(btnTextArr.size() == 0){
                Toast.makeText(DramaListActivity.this, "방송 준비 중입니다. 1시간 내에 완료됩니다.", Toast.LENGTH_SHORT).show();
            }
            btnListView.setAdapter(new DramaBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayer = new GetPlayer();
                    getPlayer.execute();
                }
            });

            mProgressDialog.dismiss();
        }
    }

    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(DramaListActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select("#player iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Intent intent = new Intent(DramaListActivity.this, GetMediFileActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(DramaListActivity.this, DramaListActivity.class);
        intent.putExtra("listUrl", listUrl);
        intent.putExtra("title", title);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("adsCnt", ""+adsCnt);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getGridView != null){
            getGridView.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }

}
